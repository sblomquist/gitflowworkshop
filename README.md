# GitFlowWorkshop
> Ever wondered what the git flow branching methodology was, or how it works?  Boy do I have a repository for you!

## Resources
[Git flow commands to git command translation](https://gist.github.com/JamesMGreene/cdd0ac49f90c987e45ac)

[Git Flow Cheatsheet](https://danielkummer.github.io/git-flow-cheatsheet/)

[Git Flow AVH Command Line Tools](https://github.com/petervanderdoes/gitflow-avh)

***

![Image of git flow opoverview](./gitFlowOverviewImage.png)

***

## Lessons

### **Lesson 00:** *Setting up your toolchain*

> Precondition: you are familiar with basic terminal usage and git commands

#### Mac (OSX)

Install Homebrew package manager
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

If already installed run brew update to ensure you are on the latest and greatest

```
brew update
```

Ensure Git is installed on your machine

```
brew install git
```

Install the [git flow](https://github.com/petervanderdoes/gitflow-avh) command line tools

```
brew install git-flow-avh
```

***

### **Lesson 01:** *Creating and initializing a git flow project*

Create a directory to house your repository if one does not currently exist

```
mkdir -p ~/Desktop/gitFlowWorkshop
```

Initialize to use git flow
```
cd ~/Desktop/gitFlowWorkshop
git flow init
```

Use the default options for branch naming. Should reflect image below

![Image of git init output](./gitInit.png)

Ensure git flow was initialized correctly and check available options

```
git flow
```

![Image of git flow options](./gitFlowOptions.png)

***

### **Lesson 02:** *Starting a new feature*

To create a new feature branch issue the following

```
git flow feature start FeatureName
```

This should create a branch named `feature/FeatureName` from the main branch which is `develop`

***

### **Lesson 03:** *Publishing a feature to a remote*

If you are working on a shared feature branch, or a repository with a remote origin, you will want to publish that branch to the repositories `origin`

```
git flow feature publish
```

***

### **Lesson 04:** *Working a feature*

Once a feature branch has been created and published, work can begin.  These branches should be worked like any other work branch.  Exception being, that these can be shared branches between developers if more than one story is needed for a feature.

Commit messages should be in the form `"[story number or feature name]:[full sentence(s) describing value add of commit.]"`

***Developer 1 committing to feature/NewFeatureFoo***

```
git commit -m "B-123: Dev A some great value in area x."
```

***Developer 2 committing to feature/NewFeatureFoo***

```
git commit -m "B-111: Man what great value added in area y."
```

***

### **Lesson 05:** *Finishing a feature*

While in development, a feature branch can be worked like any other git branch.  Committing code and push changes to the origin frequently are recommended.

```
git add .
git commit -m "FeatureName: Some cool value add."
git push
```

When you are finished with your feature, and in a team role that is responsible for merges of feature to develop, issue the following command with the feature branch checked out. This will merge the feature to `develop` and delete the branch locally.

```
git flow feature finish
```

If you are not on the feature branch you can call out what feature to finish by name

```
git flow feature FeatureName finish
```

***

### **Lesson 06:** *Creating a release*

We develop new feature on feature branches and do not merge to develop until that feature is complete.  Following this process allows to cut a release branch from develop at any time.  When the feature set for a release is finished it is time to create a release candidate for hardening.

Issue the following to create a release branch from `develop`. This branch's name is the version of code to be released.

```
git flow release start 1.0.1
```

***

### **Lesson 07:** *Publishing a release to a remote*

If you are working on a repository with a remote origin, you will want to publish the release branch to the repositories `origin`

```
git flow release 1.0.1 publish
```

***

### **Lesson 08:** *Hardening a release*

All regression and hardening activities related to this release should happen on this branch. Creating hardening branches from the release branch and merging them back into release once resolved.

The bugs fixed in hardening the release branch are back merged to develop once the release is finished.

```
git checkout -b release/1.0.1/hardening/RegressionItem release/1.0.1
```

Work the issue then merge back to release

```
git checkout release/1.0.1
git merge release/1.0.1/hardening/RegressionItem
git push
```

***

### **Lesson 09:** *Finishing a release*

Once a release branch has been regression tested and is deemed ready for release, the following command will facilitate the git actions needed to 
tag it and merge any changes back into develop.

```
git flow release finish 1.0.1 -m "Some awesome release message"
```

This will create a tag on `master` to be used to release to production, and will back merge any changes to the main `develop` branch.

***

### **Lesson 10:** *HOTFIX - When things go wrong (as they tend to do)*

When we encounter a production bug that needs to be addressed before the next release we use a hotfix release.  This will branch from the last released tag and create new hotfix release branch.  The name of the branch should be the next version number to be released.

```
git flow hotfix start 1.0.2 [tag commit id to branch from]
```

We work the issue the smae as we did in lesson 7.  Only we branch and merge to the hotfix branch.

Once the hotfix is ready to be released we issue the following:

```
git flow hotfix finish 1.0.2
```

***